#!/usr/bin/env python
import matplotlib.pyplot as pl
import numpy as np
import sys
from fastar import *
from plotline import plotline

dat = np.loadtxt("maeling1.dat", skiprows = 1)

rm  = dat[:,0]
I   = dat[:,1]

r = Rkula + Lnabbi + b*0.5 + rm
dr = dRkula + dLnabbi + db + drm
B, dB = magfield(I)
mga = (mlod*g)**(-1)
x, dx = mga*B, mga*dB

fig, ax, line, uncertain, best = plotline(
        x, r, dr, dx, "$\\frac{B}{mg}$ [\si{\\tesla\per\\newton}]",
        ylab = "Vægisarmur $r$ [\\si{\m}]"
    )
line.set_label(str(np.round(best[0], 3)) + "\si{\\ampere\meter\squared} $\\frac{B}{mg} + $" + str(np.round(best[1],5)))
ax.legend()
fig.show()
print('Vista mynd? [Y/n]')
t = input()                 
if "n" not in t:            
    fig.savefig(sys.argv[1])
pl.close(fig)               
# Teikna mynd
#X = np.vstack([x]).T
#[a, err] = np.linalg.lstsq(X, r, rcond=None)[0:2]
#
#fig, ax = pl.subplots()
#line, = ax.plot([0, 1.1*max(x)], a*[0, 1.1*max(x)])
#line.set_label(str(np.round(float(a),5)) + "\si{\\ampere\meter\squared}$\\frac{B}{mg}$")
#ax.errorbar(x, r, dr, dx, fmt=".")
#ax.set(ylabel = "Vægisarmur $r$ [\si{m}]", xlabel = "$\\frac{B}{mg}$[\si{\per\\ampere\per\meter}]")
#ax.legend()
#fig.show()
#print('Vista mynd? [Y/n]')
#t = input()
#if "n" not in t:
#    fig.savefig(sys.argv[1])
#pl.close(fig)
#print(str(err))
