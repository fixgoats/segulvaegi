#!/usr/bin/env python
import matplotlib.pyplot as pl
import numpy as np
import sys
from fastar import *
from plotline import plotline

dat = np.loadtxt("maeling2.dat", skiprows = 1)

T = dat[:,1]
dT = dat[:,3]
Ta2 = np.power(T,-2)
dTa2 = 2*Ta2*np.multiply(dT,T**(-1))
I = dat[:,2]
inerta = (0.4*mkula*Rkula**2)**(-1)

B, dB = magfield(I)
x = B*inerta*0.25*np.pi**(-2)
dx = x*dB*B**(-1)


# Teikna mynd
X = np.vstack([x]).T
[a, err] = np.linalg.lstsq(X, Ta2, rcond=None)[0:2]

fig, ax = pl.subplots()
line, = ax.plot([0, 1.1*max(x)], a*[0, 1.1*max(x)])
line.set_label(str(np.round(float(a),5))+"\si{\\ampere\meter\squared}$\\frac{B}{4\pi^2I}$")
ax.errorbar(x, Ta2, dTa2, dx, fmt=".")
ax.set(
    xlabel="$\\frac{B}{4\pi^2 I}$[\si{\kilo\gram\per\\ampere\squared\per\meter\squared}]",
    ylabel = "$T^{-2}$ [\si{\per\second\squared}]" 
)
ax.legend()
fig.show()
print('Vista mynd? [Y/n]')
t = input()
if "n" not in t:
    fig.savefig(sys.argv[1])
pl.close(fig)
print(np.sqrt(err)/len(T))
