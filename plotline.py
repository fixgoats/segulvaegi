import matplotlib.pyplot as pl
import numpy as np
from matplotlib import rcParams

def plotline(
        xdat, ydat,
        yerr = None, xerr = None,
        xlab = None, ylab = None,
        caps = 3, covar = True
    ):
    
    xlow = min(xdat)-0.1*np.abs(min(xdat))
    xhi = max(xdat) + 0.1*np.abs(max(xdat))
    linran = np.array([xlow,xhi])

    fig, ax = pl.subplots()
    ax.set(xlabel = xlab, ylabel = ylab)
    best, cov = np.polyfit(xdat,ydat, deg = 1, cov = covar)
    uncertain = str(np.sqrt(np.diagonal(cov)))

    line, = ax.plot(
            linran, best[0]*linran + best[1] 
        )
    ax.errorbar(xdat,ydat,yerr,xerr,fmt=".", capsize=caps)

    print("Hallatala " + str(best[0]) + ", skurðpunktur " + str(best[1]))
    if covar:
        print(
            "Óvissa: " + str(uncertain)
        )
    return fig, ax, line, uncertain, best
