#!/usr/bin/env python
import numpy as np
# -- Allar stærðir í SI staðaleiningum --
mu_0 = 4e-7*np.pi
g = 9.823
rspola = 1.03e-1
Dspola = 1.38e-1 # bilið milli spólanna
nspola = 195
dI = 0.1
Rkula = 2.67e-2#radius kúlu
dRkula = 3e-4
mkula = 1.415894e-1
Lnabbi = 1.235e-2
dLnabbi = 1e-4 

zkula = 0.5*Dspola # Staða kúlu á ás sem fer gegnum spólurnar
dzkula = 0.05*zkula # Gerum ráð fyrir 5% óvissu í stöðunni

hentugt = rspola**2 + zkula**2

# Fyrir fyrstu mælingu
mlod = 1.3593e-3
b = 9.5e-3
db = 5e-5
drm = 1e-4

def magfield(I):
    B = mu_0*nspola*rspola**2*np.power(hentugt,-1.5)*I
    dB = B*(np.power(I,-1)*dI + 3*zkula*np.power(hentugt, -1)*dzkula)
    return B, dB
