#!/usr/bin/env python
import matplotlib.pyplot as pl
import numpy as np
import sys
from fastar import *
from plotline import plotline

dat = np.loadtxt("maeling4.dat", skiprows = 1)

mtiny = 4.75e-4
myuge = 1.1283e-3
ny = dat[:,0]
nt = dat[:,1]
massi = mtiny*nt + myuge*ny
kraftur = massi*g
I = -dat[:,2]
Ia = np.power(I, -1)

dBdz = 1.69e-2*I
ddBdz = dBdz*0.2*Ia


# Teikna mynd
fig, ax, line, uncertain, best = plotline(
        dBdz, kraftur, xerr = ddBdz, xlab = "$z$-hnit stigulsins $\\frac{\partial B_z}{\partial z}$ [\si{\\tesla\per\meter}]",
        ylab = "Massi $M$ [\\si{\\newton}]"
    )
line.set_label(str(np.round(best[0], 3)) + "$\\frac{\partial B_z}{\partial z}$")
ax.legend()
fig.show()
print('Vista mynd? [Y/n]')
t = input()                 
if "n" not in t:            
    fig.savefig(sys.argv[1])
pl.close(fig)               
