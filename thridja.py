#!/usr/bin/env python
import matplotlib.pyplot as pl
import numpy as np
import sys
from fastar import *
from plotline import plotline

dat = np.loadtxt("maeling3.dat", skiprows = 1)

f = 6
T = dat[:,0]
dT = dat[:,2]
Ta = np.power(T,-1)
dTa = Ta*np.multiply(dT,T**(-1))
precfreq = 2*np.pi*Ta
dprfr = precfreq*T*dTa
I = dat[:,1]
inertia = 0.4*mkula*Rkula**2
L = inertia*2*np.pi*f

B, dB = magfield(I)
x = B*L**(-1)
dx = x*dB*B**(-1)


# Teikna mynd
X = np.vstack([x]).T
[a, err] = np.linalg.lstsq(X, precfreq, rcond=None)[0:2]

fig, ax = pl.subplots()
line, = ax.plot([0, 1.1*max(x)], a*[0, 1.1*max(x)])
line.set_label(str(np.round(float(a),5))+"\si{\\ampere\meter\squared}$\\frac{B}{L}$")
ax.errorbar(x, precfreq, dprfr, dx, fmt=".")
ax.set(
    xlabel="$\\frac{B}{L}$[\si{\per\\ampere\squared\per\meter\squared\per\second}]",
    ylabel = "$\Omega_p$ [\si{\per\second}]" 
)
ax.legend()
fig.show()
print('Vista mynd? [Y/n]')
t = input()
if "n" not in t:
    fig.savefig(sys.argv[1])
pl.close(fig)
print(str(err))
